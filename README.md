# datagram

this is a POC: don't use it in production. No unit testing, no secrets management, etc.

## principle

your information in a blockchain. Once created, no more way to edit this information. Users will be able to gather information directly from the sha digest in Telegram channel.

## todo 

- [ ] documentation
- [ ] deploy into a cluster
- [ ] add blockchain proof of work
- [ ] add blockchain consensus
- [ ] tests (unit, e2e)
- [ ] inject secrets
- [ ] dockerize
 
 
